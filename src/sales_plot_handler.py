import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import io
import os
from telegram import Update
from telegram.ext import CallbackContext
import calendar
from src.responce_handler import ResponseHandler

class SalesPlotHandler(ResponseHandler):
    def __init__(self, update: Update, context: CallbackContext, month_number: int = None):
        super().__init__(update, context)
        data_path = 'files/sales_with_events.csv'
        sales_data = pd.read_csv(filepath_or_buffer=data_path, delimiter=';')
        sales_data['Дата'] = pd.to_datetime(sales_data['Дата'], errors='coerce')
        sales_data['Месяц'] = sales_data['Дата'].dt.month
        if(month_number != None):
            sales_count = sales_data[sales_data['Месяц'] == month_number]
            sales_count = sales_count.groupby(sales_count['Дата'].dt.date)['Цена'].count().reset_index()
        else: 
            sales_count = sales_data.groupby(sales_data['Дата'].dt.date)['Цена'].count().reset_index()
        self.sales_data = sales_count
        self.month_number = month_number
        
    async def handle_response(self):
        """Создание и отправка графика количества продаж."""
        # Генерация графика
        sales_count = self.sales_data
        sales_count.columns = ['Дата', 'Количество продаж']
        
        fig, ax = plt.subplots(figsize=(14, 5))
        sns.lineplot(data=sales_count, x='Дата', y='Количество продаж', marker='o', color='skyblue', ax=ax)
        ax.set_xticklabels(sales_count['Дата'], rotation=45)
        if self.month_number != None:
            plt.title(f'Sales in {calendar.month_name[self.month_number]}')
        else:
            ax.set_title('Количество продаж по месяцам')
        ax.set_xlabel('Дата')
        ax.set_ylabel('Количество продаж')
        ax.grid(True)
        plt.tight_layout()
        
        # Сохранение графика в буфер
        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        buf.seek(0)

        # Отправка графика пользователю
        await self.update.message.reply_photo(photo=buf)
        plt.close()
        buf.close()
