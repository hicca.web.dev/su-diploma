from src.model_manager import ModelManager
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LinearRegression
import numpy as np
import pandas as pd
from joblib import dump

class PriceForecast(ModelManager):
    def __init__(self):
        self.model_path = 'price_model.joblib'
        super().__init__()
    
    def train_and_save_model(self):
        data_path = 'files/sales_with_events.csv'
        df = pd.read_csv(data_path, delimiter=';')
        numeric_features = ['Площадь']
        categorical_features = ['Способ покупки', 'Название проекта ЖК', 'Район']

        preprocessor = ColumnTransformer(
            transformers=[
                ('num', StandardScaler(), numeric_features),
                ('cat', OneHotEncoder(), categorical_features)
            ]
        )
        pipeline = Pipeline([
            ('preprocessor', preprocessor),
            ('regressor', LinearRegression())
        ])

        X = df.drop(columns=['Цена за квадратный метр', 'Успешна ли сделка'])
        y = df['Цена за квадратный метр']
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=42)
        pipeline.fit(X_train, y_train)
        dump(pipeline, self.model_path)
        print("Model trained and saved.")
            
    def make_prediction(self, df: pd.DataFrame):
        if not self.model:
            'No model available. Please train one first'
            return
        return self.model.predict(df)