import pandas as pd
from joblib import load
import os
from abc import ABC, abstractmethod

class ModelManager:
    def __init__(self):
        self.model = None
        if os.path.exists(self.model_path):
            self.model = load(self.model_path)
        else:
            self.train_and_save_model()
            self.model = load(self.model_path)
   
    @abstractmethod
    def train_and_save_model(self):
        pass
    
    @abstractmethod
    def make_prediction(self, df: pd.DataFrame):
        pass