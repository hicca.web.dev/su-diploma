from abc import ABC, abstractmethod
from telegram import Update
from telegram.ext import CallbackContext

class ResponseHandler(ABC):
    def __init__(self, update: Update, context: CallbackContext):
        self.update = update
        self.context = context

    @abstractmethod
    async def handle_response(self):
        """Обработка ответа пользователю."""
        pass
