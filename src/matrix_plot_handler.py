import io
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
from telegram import Update
from telegram.ext import CallbackContext
from src.responce_handler import ResponseHandler
from sklearn.preprocessing import OneHotEncoder


class MatrixPlotHandler(ResponseHandler):
    def __init__(self, update: Update, context: CallbackContext, month_number: int = None):
        super().__init__(update, context)
        data_path = 'files/sales_with_events.csv'
        sales_data = pd.read_csv(filepath_or_buffer=data_path, delimiter=';')
        sales_data.drop('Дата', axis=1, inplace=True)
        self.sales_data = sales_data
        
    async def handle_response(self):
        """Создание и отправка графика"""
        # Генерация графика
        
        encoder = OneHotEncoder(sparse_output=False, handle_unknown='ignore')
        categorical_features = ['Пол', 'В браке', 'Судим', 'Высшее', 'Есть ли кредиты активные на данный момент', 'Местный ли', 'Способ покупки', 'Название проекта ЖК', 'Район']
        
        categorical_data = encoder.fit_transform(self.sales_data[categorical_features])
        categorical_df = pd.DataFrame(categorical_data, columns=encoder.get_feature_names_out(categorical_features))

        # Объединяем закодированные категориальные столбцы с числовыми данными
        numeric_data = self.sales_data.select_dtypes(include=[np.number])
        combined_data = pd.concat([numeric_data, categorical_df], axis=1)

        # Создание корреляционной матрицы
        plt.figure(figsize=(30, 30))
        correlation_matrix = combined_data.corr()
        sns.heatmap(correlation_matrix, annot=True, cmap='coolwarm')
        plt.tight_layout()
        plt.title('Корреляционная матрица')

        # Сохранение графика в буфер
        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        buf.seek(0)

        # Отправка графика пользователю как документ
        await self.update.message.reply_document(document=buf, caption='Корреляционная матрица', filename='Матрица.png')
        plt.close()
        buf.close()