from src.model_manager import ModelManager
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, roc_auc_score
from joblib import dump

class BuyForecast(ModelManager):
    def __init__(self):
        self.model_path = 'buy_model.joblib'
        super().__init__()
    
    def train_and_save_model(self):
        data_path = 'files/sales_with_events.csv'
        df = pd.read_csv(data_path, delimiter=';')
        
        numeric_features = ['Возраст', 'Средняя зп за год', 'Кол-во в пенсионном фонде накоплений', 'Стаж работы']
        categorical_features = ['Пол', 'В браке', 'Судим', 'Высшее', 'Есть ли кредиты активные на данный момент', 'Местный ли', 'Способ покупки', 'Название проекта ЖК', 'Район']

        preprocessor = ColumnTransformer(
            transformers=[
                ('num', StandardScaler(), numeric_features),
                ('cat', OneHotEncoder(handle_unknown='ignore'), categorical_features)
            ])

        model = Pipeline(steps=[
            ('preprocessor', preprocessor),
            ('classifier', LogisticRegression())
        ])

        X = df.drop('Успешна ли сделка', axis=1)
        y = df['Успешна ли сделка']
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
        model.fit(X_train, y_train)

        y_pred = model.predict(X_test)
        accuracy = accuracy_score(y_test, y_pred)
        roc_auc = roc_auc_score(y_test, model.predict_proba(X_test)[:, 1])

        print(f"Accuracy: {accuracy:.2f}")
        print(f"ROC AUC: {roc_auc:.2f}")
        
        # Сохранение модели
        dump(model, self.model_path)
        
    #override
    def make_prediction(self, df: pd.DataFrame):
        if not self.model:
            'No model available. Please train one first'
            return
        return self.model.predict_proba(df)
