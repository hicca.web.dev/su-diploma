import logging
import numpy as np
import pandas as pd
from telegram import ReplyKeyboardMarkup, Update
from telegram.ext import Application, CommandHandler, MessageHandler, filters, CallbackContext, ConversationHandler
import os
from dotenv import load_dotenv
from src.sales_plot_handler import SalesPlotHandler
from src.matrix_plot_handler import MatrixPlotHandler
from src.price_forecast import PriceForecast
from src.buy_forecast import BuyForecast

# Загружаю переменные окружения для безопасного доступа к токену
load_dotenv()
telegram_token = os.getenv('TELEGRAM_TOKEN')

# Настройка логирования для отслеживания и диагностики
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

# Список с русскими названиями месяцев
russian_months = [
    "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
    "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"
]

# Определяю различные состояния в диалоге с пользователем для управления потоком беседы
SELECTING_ACTION, VIEWING_MONTHLY, VIEWING_ANNUAL, PRICE_PRED, CLIENT_PRED = range(5)

# Функция для клавиатуры с опциями выбора действий пользователя
def send_default_keyboard() -> ReplyKeyboardMarkup:
    keyboard = [['Результаты за весь период', 'Выбрать месяц'],
                ['Матрица зависимости параметров'],
                ['Узнать результат цены за м2'],
                ['Оценить клиента']]
    return ReplyKeyboardMarkup(keyboard, one_time_keyboard=True, resize_keyboard=True)

# Функция для отправки клавиатуры с опциями выбора действий пользователя
async def to_default(update: Update, context: CallbackContext):
    reply_markup = send_default_keyboard()
    await update.message.reply_text("Выберите следующее действие:", reply_markup=reply_markup)
    return SELECTING_ACTION

# Запуск бота и вывод приветственного сообщения с основными инструкциями
async def start(update: Update, context: CallbackContext) -> int:
    reply_markup = send_default_keyboard()
    await update.message.reply_text('Привет! Я ваш помощник. Нажмите кнопку ниже, чтобы начать.\nВыберите действие:',
                                    reply_markup=reply_markup)
    return SELECTING_ACTION

#Обработка входящих запросов
async def handle_text(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    if text == "Выбрать месяц":
        return await ask_for_month(update, context)
    elif text == 'Результаты за весь период':
        return await view_annual(update, context)
    elif 'Матрица' in text:
        return await view_matrix(update, context)
    elif 'Узнать результат цены за м2' in text:
        return await view_predict(update, context)
    elif 'Оценить клиента' in text:
        return await view_client_predict(update, context)
    else:
        reply_markup = send_default_keyboard()
        await update.message.reply_text('Пожалуйста, выберите одну из опций выше.', reply_markup=reply_markup)
        return SELECTING_ACTION

# Функция для запроса у пользователя выбора месяца
async def ask_for_month(update: Update, context: CallbackContext) -> int:
    keyboard = [[month] for month in russian_months]
    keyboard.append(['В начало'])  # Добавляем кнопку "В начало" в последнюю строку
    reply_markup = ReplyKeyboardMarkup(keyboard, one_time_keyboard=True, resize_keyboard=True)
    await update.message.reply_text('Пожалуйста, выберите месяц:', reply_markup=reply_markup)
    return VIEWING_MONTHLY

# Функция для запроса у пользователя выбора месяца
async def view_monthly(update: Update, context: CallbackContext) -> int:
    try:
        month_number = russian_months.index(update.message.text) + 1
        await update.message.reply_text(f"Данные за месяц {update.message.text}...")
        handler = SalesPlotHandler(update, context, month_number)
        await handler.handle_response()
    except ValueError:
        await update.message.reply_text("Введите корректное название месяца:")
        return VIEWING_MONTHLY

    reply_markup = send_default_keyboard()
    await update.message.reply_text("Выберите следующее действие:", reply_markup=reply_markup)
    return SELECTING_ACTION

# Функция для отправки отчета за весь период
async def view_annual(update: Update, context: CallbackContext) -> int:
    await update.message.reply_text("Здесь будут данные за весь период...")
    handler = SalesPlotHandler(update, context)
    await handler.handle_response()
    return await to_default(update, context)

async def view_matrix(update: Update, context: CallbackContext) -> int:
    await update.message.reply_text("Здесь будет матрица зависимостей...")
    handler = MatrixPlotHandler(update, context)
    await handler.handle_response()
    return await to_default(update, context)

async def cancel(update: Update, context: CallbackContext) -> int:
    await update.message.reply_text('Операция отменена.')
    return ConversationHandler.END

async def view_predict(update: Update, context: CallbackContext) -> int:
    await update.message.reply_text(
    "Введите параметры через запятую в следующем порядке:\n"
    "Площадь, Способ покупки, Название проекта ЖК, Район"
    )
    return PRICE_PRED

async def view_client_predict(update: Update, context: CallbackContext) -> int:
    await update.message.reply_text(
    "Введите параметры через запятую в следующем порядке:\n"
    "Возраст, Средняя зп за год, Кол-во в пенсионном фонде накоплений,\nСтаж работы, Пол, В браке, Судим,\nЕсть ли высшее,\nЕсть ли кредиты активные на данный момент,\nМестный ли, Способ покупки, Название проекта ЖК, Район"
    )
    return CLIENT_PRED

def get_input_for_predict(update: Update, context: CallbackContext, columns: list) -> pd.DataFrame:
    # Получаем текст сообщения
    input_text = update.message.text
    # Разбиваем текст на элементы, обрезая пробелы в начале и конце каждого элемента
    data = [item.strip() for item in input_text.split(',')]
    
    # Проверяем, что количество элементов соответствует ожидаемому количеству столбцов
    if len(data) != len(columns):
        # В случае несоответствия, отправляем сообщение об ошибке пользователю
        update.message.reply_text("Некорректный ввод данных. Пожалуйста, введите данные, разделенные запятыми, для всех следующих полей: " + ', '.join(columns))
        return pd.DataFrame()  # Возвращаем пустой DataFrame
    
    # Создаем DataFrame с одной строкой из входных данных
    df = pd.DataFrame([data], columns=columns)
    return df

async def predict(update: Update, context: CallbackContext) -> int:
    columns = ['Площадь','Способ покупки', 'Название проекта ЖК', 'Район']
    df = get_input_for_predict(update, context, columns)
    pred = PriceForecast().make_prediction(df)[0]
    await update.message.reply_text(f"Предсказанная цена за квадратный метр: {pred:.2f}")
    return await to_default(update, context)

async def predict_client(update: Update, context: CallbackContext) -> int:
    columns = ['Возраст', 'Средняя зп за год', 'Кол-во в пенсионном фонде накоплений', 'Стаж работы', 'Пол', 'В браке', 'Судим', 'Высшее', 'Есть ли кредиты активные на данный момент', 'Местный ли', 'Способ покупки', 'Название проекта ЖК', 'Район']
    df = get_input_for_predict(update, context, columns)
    model = BuyForecast()
    pred = model.make_prediction(df)[0, 1]
    
    if pred > 0.5:
        response = f"Клиент скорее всего купит квартиру. Вероятность: {pred:.2f}"
    else:
        response = f"Клиент скорее всего не купит квартиру. Вероятность: {pred:.2f}"

    await update.message.reply_text(response)
    return await to_default(update, context)

#Распределение команд
def main() -> None:
    application = Application.builder().token(telegram_token).build()

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],
        states={
            SELECTING_ACTION: [MessageHandler(filters.TEXT & ~filters.COMMAND, handle_text)],
            VIEWING_MONTHLY: [MessageHandler(filters.TEXT & ~filters.COMMAND, view_monthly)],
            VIEWING_ANNUAL: [MessageHandler(filters.TEXT & ~filters.COMMAND, view_annual)],
            PRICE_PRED: [MessageHandler(filters.TEXT & ~filters.COMMAND, predict)],
            CLIENT_PRED: [MessageHandler(filters.TEXT & ~filters.COMMAND, predict_client)],
        },
        fallbacks=[CommandHandler('cancel', cancel)]
    )

    application.add_handler(conv_handler)
    application.run_polling()

if __name__ == '__main__':
    print("Текущая рабочая директория:", os.getcwd())
    main()
